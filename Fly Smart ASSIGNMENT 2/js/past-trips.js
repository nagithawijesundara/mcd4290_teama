function displayTrips(trips) {
    let returnHTML = "";
    for (let i = 0; i < trips.length; i++) {
        let journey = '';
        for (const airport of trips[i].getAirportList()) {
            journey += journey === '' ? airport.name : '<br/> -> ' + airport.name;
        }
        returnHTML += `<tr>
                       <td>${trips[i].getId()}</td>
                       <td>${trips[i].getDate()}</td>
                       <td>${trips[i].getCountry()}</td>
                       <td>${journey}</td>
                       <td>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"
                                    type="button" onclick="showTrip(${trips[i].getId()})">
                                Show
                            </button>
                       </td>
                       <td>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                                    type="button" onclick="deleteTrip(${trips[i].getId()})">
                                Delete
                            </button>
                       </td>
                       </tr>`;
    }
    return returnHTML;
}

function showTrip(id) {
    clearMap();
    let trip = trips.getTripById(id);
    for (let i = 0; i < trip.getAirportList().length; i++) {
        map.flyTo({center: [trip.getAirportList()[i].longitude, trip.getAirportList()[i].latitude], zoom: 8});
        mapMarkers.push(new mapboxgl.Marker().setLngLat([trip.getAirportList()[i].longitude, trip.getAirportList()[i].latitude]).addTo(map).setPopup(new mapboxgl.Popup().setText(`${trip.getAirportList()[i].name}`).addTo(map)));
        if (i > 0) {
            mapLayers.push(`${trip.getAirportList()[i - 1].name + ' to ' + trip.getAirportList()[i].name + ' Line'}`);
            map.addSource(`${trip.getAirportList()[i - 1].name + ' to ' + trip.getAirportList()[i].name + ' Line'}`,
                {
                    type: 'geojson',
                    data:
                        {
                            type: 'Feature',
                            properties: {
                                description: `${trip.getAirportList()[i - 1].name + ' to ' + trip.getAirportList()[i].name}`
                            },
                            geometry:
                                {
                                    type: 'LineString',
                                    coordinates: [
                                        [trip.getAirportList()[i - 1].longitude, trip.getAirportList()[i - 1].latitude],
                                        [trip.getAirportList()[i].longitude, trip.getAirportList()[i].latitude]
                                    ]
                                }
                        }
                })
            map.addLayer({
                id: `${trip.getAirportList()[i - 1].name + ' to ' + trip.getAirportList()[i].name + ' Line'}`,
                type: 'line',
                source: `${trip.getAirportList()[i - 1].name + ' to ' + trip.getAirportList()[i].name + ' Line'}`,
                paint: {
                    'line-color': '#888',
                    'line-width': 8
                }
            })
        }
    }
}

function deleteTrip(id) {
    if (confirm("Do you want to delete trip?")) {
        let trip = trips.getTripById(id);
        trips.removeTrip(trip.getId());
        updateLocalStorage(TRIP_DATA_KEY, trips);
        alert("Successfully deleted trip");
        tableBody.innerHTML = displayTrips(trips.getTrips().filter(trip => new Date(trip.getDate()) < new Date()));
    }
}

function clearMap() {
    mapMarkers.forEach((mapMarker) => mapMarker.remove());
    mapLayers.forEach((mapLayer) => {
        let hasPoly = map.getLayer(mapLayer)
        if (hasPoly !== undefined) {
            map.removeLayer(mapLayer);
            map.removeSource(mapLayer);
        }
    });
    mapLayers = [];
    mapMarkers = [];
    domesticRoutes = [];
    destinationAirports = [];
}

tableBody.innerHTML = displayTrips(trips.getTrips().filter(trip => new Date(trip.getDate()) < new Date()));
mapboxgl.accessToken = 'pk.eyJ1IjoibmF2aW5kdTU1IiwiYSI6ImNsMmtlczJ2ajE2NzczYnA5MWZyZjQwZGQifQ.XQ5-1kiL0oFyOlDE63VHgg';
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 0
});
let domesticRoutes = [],
    mapMarkers = [],
    mapLayers = [];
