function getCountry() {
    let innerHtml = `<option value="">Select a country</option>`;
    for (const country of countryData) {
        innerHtml += `<option value="${country}">${country}</option>`
    }
    return innerHtml;
}

function changeCountry() {
    let url = "https://eng1003.monash/OpenFlights/airports/";
    let callback = `getAirport`;
    let data = {
        country: `${country.value}`,
        callback: callback
    };
    webServiceRequest(url, data);
}

function getAirport(airports) {
    let innerHtml = '';
    domesticAirports = airports;
    for (const airport of airports) {
        const value = JSON.stringify(airport);
        innerHtml += `<option value='${value}'>${airport.name}</option>`
    }
    sourceAirport.innerHTML = innerHtml;
    currentJourneyContent = '';
    currentJourneyAirportList = [];
    currentJourney.innerHTML = currentJourneyContent;
    confirmed = false;
    changeAirport();
}

function changeAirport() {
    clearMap();
    let airport = JSON.parse(sourceAirport.value);
    currentAirport.innerHTML = airport.name;
    let url = "https://eng1003.monash/OpenFlights/routes/";
    let callback = `getRoutes`;
    let data = {
        sourceAirport: `${airport.airportId}`,
        callback: callback
    };
    currentJourneyContent = '';
    currentJourneyAirportList = [];
    currentJourney.innerHTML = currentJourneyContent;
    confirmed = false;
    webServiceRequest(url, data);
}

function getRoutes(routes) {
    let airport = destinationAirport.value !== "" ? JSON.parse(destinationAirport.value) : JSON.parse(sourceAirport.value);
    map.flyTo({center: [airport.longitude, airport.latitude], zoom: 8});
    mapMarkers.push(new mapboxgl.Marker().setLngLat([airport.longitude, airport.latitude]).addTo(map).setPopup(new mapboxgl.Popup().setText(`${airport.name}`).addTo(map)));
    for (const domesticAirport of domesticAirports) {
        let filteredRoutes = routes.filter(route => `${route.destinationAirportId}` === domesticAirport.airportId);
        if (filteredRoutes.length > 0) {
            domesticRoutes = domesticRoutes.concat(filteredRoutes[0]);
        }
    }
    let innerHtml = `<option value="">Select a destination airport</option>`;
    for (const domesticRoute of domesticRoutes) {
        let filteredAirports = domesticAirports.filter(domesticAirport => `${domesticRoute.destinationAirportId}` === domesticAirport.airportId);
        if (filteredAirports.length > 0) {
            destinationAirports.push(filteredAirports[0]);
            const value = JSON.stringify(filteredAirports[0]);
            innerHtml += `<option value='${value}'>${filteredAirports[0].name}</option>`;
            mapMarkers.push(new mapboxgl.Marker().setLngLat([filteredAirports[0].longitude, filteredAirports[0].latitude]).addTo(map).setPopup(new mapboxgl.Popup().setText(`${filteredAirports[0].name}`).addTo(map)));
            mapLayers.push(`${airport.name + ' to ' + filteredAirports[0].name + ' Line'}`);
            map.addSource(`${airport.name + ' to ' + filteredAirports[0].name + ' Line'}`,
                {
                    type: 'geojson',
                    data:
                        {
                            type: 'Feature',
                            properties: {
                                description: `${airport.name + ' to ' + filteredAirports[0].name}`
                            },
                            geometry:
                                {
                                    type: 'LineString',
                                    coordinates: [
                                        [airport.longitude, airport.latitude],
                                        [filteredAirports[0].longitude, filteredAirports[0].latitude]
                                    ]
                                }
                        }
                })
            map.addLayer({
                id: `${airport.name + ' to ' + filteredAirports[0].name + ' Line'}`,
                type: 'line',
                source: `${airport.name + ' to ' + filteredAirports[0].name + ' Line'}`,
                paint: {
                    'line-color': '#888',
                    'line-width': 8
                }
            })
        }
    }
    destinationAirport.innerHTML = innerHtml;
}

function clearMap() {
    mapMarkers.forEach((mapMarker) => mapMarker.remove());
    mapLayers.forEach((mapLayer) => {
        let hasPoly = map.getLayer(mapLayer)
        if (hasPoly !== undefined) {
            map.removeLayer(mapLayer);
            map.removeSource(mapLayer);
        }
    });
    mapLayers = [];
    mapMarkers = [];
    domesticRoutes = [];
    destinationAirports = [];
}

function clearForm() {
    form.reset();
    sourceAirport.innerHTML = null;
    destinationAirport.innerHTML = null;
    currentAirport.innerHTML = null;
    currentJourney.innerHTML = null;
    confirmed = false;
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 0
    });
}

function changeDestinationAirport() {
    clearMap();
    let airport = JSON.parse(destinationAirport.value),
        sourceAirportData = JSON.parse(sourceAirport.value);
    if (currentJourneyAirportList.length <= 0) {
        currentJourneyAirportList.push(sourceAirportData);
    }
    currentJourneyAirportList.push(airport);
    currentJourneyContent += currentJourneyContent === '' ? sourceAirportData.name : '';
    currentJourneyContent += ' -> ' + airport.name;
    currentJourney.innerHTML = currentJourneyContent;
    currentAirport.innerHTML = airport.name;
    confirmed = false;
    let url = "https://eng1003.monash/OpenFlights/routes/";
    let callback = `getRoutes`;
    let data = {
        sourceAirport: `${airport.airportId}`,
        callback: callback
    };
    webServiceRequest(url, data);
}

function submitTrip() {
    if (confirmed) {
        let id = (trips.getCount() + 1);
        trips.addTrip(id, date.value, country.value, sourceAirport.value, currentJourneyAirportList);
        updateLocalStorage(TRIP_DATA_KEY, trips);
        window.location = "scheduled-trips.html";
    } else {
        clearMap();
        for (let i = 0; i < currentJourneyAirportList.length; i++) {
            mapMarkers.push(new mapboxgl.Marker().setLngLat([currentJourneyAirportList[i].longitude, currentJourneyAirportList[i].latitude]).addTo(map).setPopup(new mapboxgl.Popup().setText(`${currentJourneyAirportList[i].name}`).addTo(map)));
            if (i > 0) {
                mapLayers.push(`${currentJourneyAirportList[i - 1].name + ' to ' + currentJourneyAirportList[i].name + ' Line'}`);
                map.addSource(`${currentJourneyAirportList[i - 1].name + ' to ' + currentJourneyAirportList[i].name + ' Line'}`,
                    {
                        type: 'geojson',
                        data:
                            {
                                type: 'Feature',
                                properties: {
                                    description: `${currentJourneyAirportList[i - 1].name + ' to ' + currentJourneyAirportList[i].name}`
                                },
                                geometry:
                                    {
                                        type: 'LineString',
                                        coordinates: [
                                            [currentJourneyAirportList[i - 1].longitude, currentJourneyAirportList[i - 1].latitude],
                                            [currentJourneyAirportList[i].longitude, currentJourneyAirportList[i].latitude]
                                        ]
                                    }
                            }
                    })
                map.addLayer({
                    id: `${currentJourneyAirportList[i - 1].name + ' to ' + currentJourneyAirportList[i].name + ' Line'}`,
                    type: 'line',
                    source: `${currentJourneyAirportList[i - 1].name + ' to ' + currentJourneyAirportList[i].name + ' Line'}`,
                    paint: {
                        'line-color': '#888',
                        'line-width': 8
                    }
                })
            }
        }
        confirmed = true
    }
}

country.innerHTML = getCountry();
let domesticAirports = [],
    destinationAirports = [],
    domesticRoutes = [],
    currentJourneyContent = '',
    currentJourneyAirportList = [],
    mapMarkers = [],
    mapLayers = [],
    confirmed = false;

mapboxgl.accessToken = 'pk.eyJ1IjoibmF2aW5kdTU1IiwiYSI6ImNsMmtlczJ2ajE2NzczYnA5MWZyZjQwZGQifQ.XQ5-1kiL0oFyOlDE63VHgg';
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 0
});
