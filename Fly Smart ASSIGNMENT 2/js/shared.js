"use strict";
// Constants used as KEYS for LocalStorage
const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_DATA_KEY = "tripLocalData";

class Trip {
    constructor(id = "") {
        this.id = id;
    }

    getId() {
        return this.id;
    }

    getDate() {
        return this.date;
    }

    setDate(date = "") {
        this.date = date;
    }

    getCountry() {
        return this.country;
    }

    setCountry(country = "") {
        this.country = country;
    }

    getSourceAirport() {
        return this.sourceAirport;
    }

    setSourceAirport(sourceAirport = {}) {
        this.sourceAirport = sourceAirport;
    }

    getAirportList() {
        return this.airportList;
    }

    setAirportList(airportList = []) {
        this.airportList = airportList;
    }

    fromData(data) {
        this.setDate(data.date);
        this.setCountry(data.country);
        this.setSourceAirport(data.sourceAirport);
        this.setAirportList(data.airportList);
    }
}

class TripList {
    constructor() {
        this.trips = [];
    }

    getTrips() {
        return this.trips;
    }

    getCount() {
        return this.trips.length;
    }

    addTrip(id = "", date, country, sourceAirport, airportList) {
        let trip = new Trip(id);
        trip.setDate(date);
        trip.setCountry(country);
        trip.setSourceAirport(sourceAirport);
        trip.setAirportList(airportList);
        this.trips.push(trip);
    }

    getTrip(index) {
        return this.trips[index];
    }

    getTripById(id) {
        const index = this.trips.findIndex(trip => {
            return trip.getId() === id;
        });
        return this.trips[index];
    }

    removeTrip(id = "") {
        const index = this.trips.findIndex(trip => {
            return trip.getId() === id;
        });
        this.trips.splice(index, 1);
    }

    fromData(data) {
        let trips = [];
        for (const trip of data.trips) {
            let newTrip = new Trip(trip.id);
            newTrip.fromData(trip);
            trips.push(newTrip);
        }
        this.trips = trips;
    }
}

/**
 * checkIfDataExistsLocalStorage function
 * Used to check if any data in LS exists at a specific key
 * @param {string} key LS Key to be used
 * @returns true or false representing if data exists at key in LS
 */
function checkIfDataExistsLocalStorage(key) {
    if (localStorage.getItem(key) != null) {
        return true;
    }
    return false;
}

/**
 * getDataLocalStorage function
 * Used to retrieve data from LS at a specific key.
 * @param {string} key LS Key to be used
 * @returns data from LS in JS format
 */
function getDataLocalStorage(key) {
    let data = localStorage.getItem(key);
    try {
        data = JSON.parse(data);
    } catch (err) {
    } finally {
        return data;
    }
}

/**
 * updateLocalStorage function
 * Used to store JS data in LS at a specific key
 * @param {string} key LS key to be used
 * @param {any} data data to be stored
 */
function updateLocalStorage(key, data) {
    let json = JSON.stringify(data);
    localStorage.setItem(key, json);
}


// Global LockerList instance variable
let trips = new TripList();
// Check if data available in LS before continuing
if (checkIfDataExistsLocalStorage(TRIP_DATA_KEY)) {
    // If data exists, retrieve it
    let data = getDataLocalStorage(TRIP_DATA_KEY);
    // Restore data into locker
    trips.fromData(data);
}
